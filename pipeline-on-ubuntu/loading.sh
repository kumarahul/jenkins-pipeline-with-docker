#!/bin/bash
n=`expr $1 / 2`

echo -ne ' [#####                  ]  (33%)\r'
sleep $n
echo -ne ' [#############          ]  (66%)\r'
sleep $n
echo -ne ' [#######################]  (100%)\r'
echo -ne '\n'

